const state = {}
const getters = {}
const mutations = {}
const actions = {
  GENERATE_IPAY_KEY () {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/ipay/generate-key').then(resp => {
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}