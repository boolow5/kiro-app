import Vue from '../../main'

const state = {
  searchAddress: {
    administrative_area_level_1: '',
    country: '',
    lat: null,
    locality: '',
    long: null,
    route: '',
    listing_filters: ''
  },
  nearbyHomes: [],
  search_radius: 1000,
  lastCoords: {}
}

const getters = {
  searchAddress: state => state.searchAddress,
  searchAddressCoords: state => {
    return {
      lat: state.searchAddress.lat,
      long: state.searchAddress.long,
    }
  },
  nearbyHomes: state => state.nearbyHomes,
  search_radius: state => state.search_radius,
  listing_filters: state => state.listing_filters,
  lastCoords: state => state.lastCoords,
  storedCoords: () => JSON.parse(localStorage.getItem('lastCoords')),
  stillInLastCoords: state => {
    if (!state.lastCoords) {
      return false
    }
    let lastCoords = JSON.parse(localStorage.getItem('lastCoords'))
    if (!lastCoords) {
      return false
    }
    return state.lastCoords.lat === lastCoords.lat && state.lastCoords.long === lastCoords.long
  }
}

const mutations = {
  SET_SEARCH_RADIUS (state, payload) {
    state.search_radius = parseFloat(payload)
  },
  SET_SEARCH_ADDRESS (state, payload) {
    console.log('SET_SEARCH_ADDRESS', payload)
    state.searchAddress = payload
  },
  SET_NEARBY_HOMES (state, payload) {
    state.nearbyHomes = payload
  },
  SET_LISTING_FILTERS (state, payload) {
    state.listing_filters = payload
  },
  SET_LAST_COORDS (state, payload) {
    state.lastCoords = payload
    localStorage.setItem('lastCoords', JSON.stringify(state.lastCoords))
  }
}

const actions = {
  GET_NEARBY_HOMES ({state}, payload) {
    return new Promise((resolve, reject) => {
      console.log('GET_NEARBY_HOMES', payload)
      let query = ''
      if (payload && typeof payload.lat === 'number' && typeof payload.lat === 'number') {
        query = `?lat=${payload.lat}&long=${payload.long}&max_radius=${state.search_radius}`
      }
      if (payload.has_filters && state.listing_filters) {
        query += state.listing_filters
      }
      Vue.$http.get('/api/v1/listing/nearby' + query).then(resp => {
        console.log('GET_NEARBY_HOMES response', resp)
        if (resp && resp.data) {
          resolve(resp.data)
        } else {
          resolve(resp)
        }
      }).catch(err => {
        console.log('GET_NEARBY_HOMES error', err)
        reject(err.data)
      })
    })
  },
  GEOCODE (_, payload) {
    return new Promise((resolve, reject) => {
      Vue.$http.post('/api/v1/location/geocode', payload).then(resp => {
        console.log('GEOCODE response', resp)
        if (resp && resp.data) {
          resolve(resp.data)
        } else {
          resolve(resp)
        }
      }).catch(err => {
        reject(err.data)
      })
    })
  },
  REVERSE_GEOCODE (_, payload) {
    return new Promise((resolve, reject) => {
      Vue.$http.post('/api/v1/location/reversegeocode', payload).then(resp => {
        console.log('REVERSE_GEOCODE response', resp)
        if (resp && resp.data) {
          resolve(resp.data)
        } else {
          resolve(resp)
        }
      }).catch(err => {
        reject(err.data)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}