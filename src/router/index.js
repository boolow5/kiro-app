import VueRouter from 'vue-router'

import {isLoggedIn} from '../store/modules/user'

import Home from '../components/Home.vue'
import Dashboard from '../components/Dashboard.vue'
import Login from '../components/Login.vue'
import Signup from '../components/Signup.vue'
import Profile from '../components/Profile.vue'
import Subscription from '../components/Subscription'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { name: 'Home', path: '/', component: Home, meta: {title: 'Home', authRequired: false} },
    { name: 'Dashboard', path: '/dashboard', component: Dashboard, meta: {title: 'Dashboard', authRequired: true} },
    { name: 'Login', path: '/login', component: Login, meta: {title: 'Login', noLayout: true} },
    { name: 'Signup', path: '/signup', component: Signup, meta: {title: 'Signup', noLayout: true} },
    { name: 'Profile', path: '/profile', component: Profile, meta: {title: 'Profile', authRequired: true} },
    { name: 'Subscription', path: '/subscription', component: Subscription, meta: {title: 'Subscription', authRequired: true} },
  ]
})

router.beforeEach((to, from, next) => {
  console.log('beforeEach', {to, from, next})
  to.matched.some(route => {
    document.title = route.meta.title
    if (route.meta.authRequired && !isLoggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  })
})

export default router