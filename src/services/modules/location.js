import q from 'q'
import axios from 'axios'
import common from './common'

var getCurrentCoordinates = function () {
  var deferred = q.defer()
  if (!window.navigator || !window.navigator.geolocation || !window.navigator.geolocation.getCurrentPosition) {
    deferred.reject(new Error('Geolocation service is not available'))
  }
  window.navigator.geolocation.getCurrentPosition(function (position) {
    console.log('window.navigator.geolocation.getCurrentPosition', position)
    let lat = null
    let long = null
    let accuracy = null
    let speed = null
    let altitude = null
    let altitudeAccuracy = null
    if (position.coords && typeof position.coords.latitude === 'number' && typeof position.coords.longitude === 'number') {
      lat = position.coords.latitude
      long = position.coords.longitude
      accuracy = position.coords.accuracy
      speed = position.coords.speed
      altitude = position.coords.altitude
      altitudeAccuracy = position.coords.altitudeAccuracy
    }
    deferred.resolve({
      lat,
      long,
      accuracy,
      speed,
      altitude,
      altitudeAccuracy
    })
  }, function (err) {
    switch (err.code) {
      case 0:
        deferred.reject(new Error('Location error unknown'))
        break
      case 1:
        deferred.reject(new Error('Location permission denied'))
        break
      case 2:
        deferred.reject(new Error('Location data not found'))
        break
      case 3:
        deferred.reject(new Error('Location time out'))
        break
      default:
        deferred.reject(err)
    }
  })
  return deferred.promise
}

var Geocode = function (address) {
  var deferred = q.defer()
  if (!address) {
    deferred.reject(new Error('invalid address'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/signup'),
    data: {address}
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var ReverseGeocode = function (coords) {
  var deferred = q.defer()
  if (!coords) {
    deferred.reject(new Error('invalid coordinates'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/reversegeocode'),
    data: coords
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var CalculateDistance = function (coords1, coords2) {
  var deferred = q.defer()
  if (!coords1 || !coords2) {
    deferred.reject(new Error('invalid coordinates'))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/calculate-distance'),
    data: {from: coords1, to: coords2},
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var NearbyDistricts = function (lat, long, radius) {
  var deferred = q.defer()
  if (!lat || !long || !radius) {
    deferred.reject(new Error(`invalid data: ${{lat, long, radius}}`))
  }
  axios({
    method: 'POST',
    url: common.getFullURL('/nearby-districts'),
    data: {lat, long, radius},
    headers: { 'Authorization': '' + common.getToken() || localStorage.getItem('user_token') }
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  getCurrentCoordinates,
  Geocode,
  ReverseGeocode,
  CalculateDistance,
  NearbyDistricts
}
