const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
  UPLOAD_IMAGE (_, payload) {
    return new Promise((resolve, reject) => {
      console.log('UPLOAD_IMAGE payload', payload)
      let query = `?targetID=${payload.targetID}&targetItem=house`
      var formData = new FormData()
      formData.append('file', payload.file)
      window.VueInstance.$http.post('/api/v1/image/upload'+query, formData, {emulateJSON: true}).then(resp => {
        console.log('UPLOAD_IMAGE response', resp.data.image)
        resolve(resp.data.image)
      }).catch(err => {
        console.log('UPLOAD_IMAGE response', err)
        reject(err)
      })
    })
  },
  UPLOAD_IMAGES (_, payload) {
    return new Promise((resolve, reject) => {
      console.log('UPLOAD_IMAGES payload', payload)
      let query = `?targetID=${payload.targetID}&targetItem=house`
      let formData = new FormData()
      let files = []
      for (var i = 0; i < payload.files.length; i++) {
        console.log('payload-'+i, payload.files[i].file)
        files.push(payload.files[i].file)
      }
      formData.append('files', payload.files)
      console.log('UPLOAD_IMAGES form-data', formData)
      window.VueInstance.$http.post(
        '/api/v1/images/upload' + query,
        formData,
        {
          headers: {
              'Content-Type': 'multipart/form-data'
          },
          emulateJSON: true
       }
      ).then(resp => {
        console.log('UPLOAD_IMAGES response ', resp)
        resolve(resp)
      }).catch(err => {
        console.log('UPLOAD_IMAGES error', err.data)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}