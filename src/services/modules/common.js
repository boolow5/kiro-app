const REMOTE_URL = 'https://api.qaado.com'
const REMOTE_SSE = 'https://sse.qaado.com'
const LOCAL_URL = 'http://localhost:8082'
const LOCAL_SSE = 'http://localhost:8083'
const VERSION = '/api/v1'
var BASE_URL = REMOTE_URL
var BASE_SSE = REMOTE_SSE
const LOCAL = true
if (LOCAL) {
  BASE_URL = LOCAL_URL
  BASE_SSE = LOCAL_SSE
}

var getFullURL = function (path, version) {
  if (!version) {
    version = VERSION
  }
  if (!path) {
    return join(BASE_URL, version || '')
  }
  return join(BASE_URL, version, path)
}

var getBareURL = function (path) {
  if (!path) {
    return BASE_URL
  }
  return join(BASE_URL, path)
}

var getSSEDomain = function (path) {
  if (!path) {
    return BASE_SSE
  }
  return join(BASE_SSE, path)
}

var join = function () {
  if (!arguments) {
    return ''
  }
  if (arguments.length < 1) {
    return ''
  }
  let url = ''
  for (let i = 0; i < arguments.length; i++) {
    let arg = removeTrailingSlash(arguments[i])
    if (i === 0) {
      url += arg
    } else if (startsWith(arg, '/')) {
      url += arg
    } else {
      url += `/${arg}`
    }
  }
  return url
}

var AppendQuery = function (url, key, value) {
  if (!url) {
    url = ''
  }
  if (!key) {
    return url
  }
  if (url.indexOf('?') === -1) {
    url += '?'
  }
  let lastChar = url[url.length - 1]
  console.log('lastChar', lastChar)
  if (lastChar !== '&' && lastChar !== '?') {
    url += '&'
  }
  url += key
  if (value) {
    url += '=' + value
  }
  return url
}

var removeTrailingSlash = function (str) {
  if (!str) {
    return str
  }
  if (typeof str !== 'string') {
    return str
  }
  if (str.length < 1) {
    return ''
  }
  let lastIndex = str.length - 1
  if (str[lastIndex] === '/') {
    return str.slice(0, lastIndex)
  }
  return str
}

var startsWith = function (str, sub) {
  if (typeof str !== 'string') {
    return false
  }
  if (str.length < 1) {
    return false
  }
  return str.indexOf(sub) === 0
}

var getToken = function () {
  return window.localStorage.getItem('user_token') || ''
}

const handleError = function (err) {
  console.log('handleError', err)
}

export default {
  getFullURL,
  getToken,
  getSSEDomain,
  AppendQuery,
  getBareURL,
  handleError
}
