const state = {
  drawer: true,
  layout: true,
  loading: {enable: false, msg: 'Initializing...'},
  listModeHome: true,
  defaultImage: 'not-found.png',
  filter_sidebar: false
}

const getters = {
  drawer: (state) => state.drawer,
  layout: (state) => state.layout,
  app_loading: (state) => state.loading,
  listModeHome: (state) => state.listModeHome,
  defaultImage: (state) => state.defaultImage,
  filter_sidebar: (state) => state.filter_sidebar
}

const mutations = {
  DRAWER (state, value) {
    state.drawer = value === true
  },
  APP_LOADING (state, value) {
    console.log('APP_LOADING', value)
    if (typeof value === 'boolean') {
      state.loading.enable = value
    } else if (typeof value === 'string') {
      state.loading.msg = value
    } else if (typeof value === 'object' && value.hasOwnProperty('enable')) {
      state.loading = value
    }
  },
  IS_LIST_MODE_HOME (state, value) {
    state.listModeHome = value === true
  },
  SET_FILTER_SIDEBAR (state, value) {
    state.filter_sidebar = value === true
  }
}

const actions = {
  CLEAR_ALL (context) {
    context.commit('CLEAR_USER_DATA')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}