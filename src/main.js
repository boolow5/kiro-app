import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from './store'
import router from './router'
import i18n from 'vuex-i18n'
import vSelect from 'vue-select'
import {dates, strings} from './mixins'
import Dialog from "vuejs-dialog";
// import VuejsDialogMixin from 'vuejs-dialog/vuejs-dialog-mixin.min.js'; // only needed in custom components

// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import './assets/styles.css'

Vue.use(i18n.plugin, store)
Vue.component('vue-select', vSelect)
Vue.mixin(dates)
Vue.mixin(strings)
window.baseURL = (process.env.NODE_ENV === 'development' ? 'http://localhost:8082' : 'http://kiro.bolow.me')

Vue.use(VueRouter)
var http = axios.create({
  baseURL: window.baseURL,
  timeout: 15 * 1000,
  headers: {
      'Content-Type': 'application/json'
  }
})
Vue.use(Dialog)

http.interceptors.request.use(config => {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    config.headers['Authorization'] = auth.jwt_token
  }
  return config
}, err => {
  console.log('axiso error', err)
})
Vue.prototype.$http = http
Vue.config.productionTip = false

export const EventBus = new Vue()

export default new Vue({
  store,
  router,
  render: h => h(App),
  created () {
    window.VueInstance = this
  }
}).$mount('#app')
