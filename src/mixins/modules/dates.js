const toLocaleString = function (arg) {
  if (typeof arg === 'string') {
    return new Date(arg).toLocaleString('en-GB')
  }
  if (typeof arg === 'object' && arg.consturctor.name === 'Date') {
    return arg.toLocaleString('en-GB')
  } else if (typeof arg === 'number') {
    return arg.toLocaleString('en-GB')
  }
  return arg
}

const toLocaleDateString = function (date) {
  if (typeof date === 'string') {
    return new Date(date).toLocaleDateString('en-GB')
  }
  if (typeof date === 'object' && date.consturctor.name === 'Date') {
    return date.toLocaleDateString('en-GB')
  }
  return date
}

const toDateDiffString = function (date) {
  let diff = 0
  let now = new Date()
  if (typeof date === 'string') {
    date = new Date(date)
    diff = now.getTime() - date.getTime()
  } else if (typeof date === 'object' && date.consturctor.name === 'Date') {
    diff = now.getTime() - date.getTime()
  } else {
    return date
  }

  let isBefore = diff > 0
  let durType = 'year'
  // diff in hours
  diff = Math.abs(Math.ceil(diff / (1000 * 3600)))
  // more than year
  if (diff > 365 * 24) {
    diff = Math.ceil(diff / 365)
    durType = diff === 1 ? 'year' : 'years'
    return (isBefore ? `${diff} ${durType} ago` : `after ${diff} ${durType}`)
  } else if (diff > 30 * 24) { // more than a month
    diff = Math.ceil(diff / 30 * 24)
    durType = diff === 1 ? 'month' : 'months'
    return (isBefore ? `${diff} ${durType} ago` : `after ${diff} ${durType}`)
  } else if (diff > 24) { // more than a day
    diff = Math.ceil(diff / 24)
    durType = diff === 1 ? 'day' : 'days'
    return (isBefore ? `${diff} ${durType} ago` : `after ${diff} ${durType}`)
  }
  diff = Math.ceil(diff)
  durType = diff === 1 ? 'hour' : 'hours'
  return (isBefore ? `${diff} ${durType} ago` : `after ${Math.ceil(diff)} ${durType}`)
}

const shortNumber = function (num) {
  let result = num
  if (typeof num !== 'number') {
    result = Number(num)
    if (isNaN(result)) {
      return num
    }
  }
  if (result < 1000) {
    return result
  }
  if (result < 1000000) {
    return `${(result / 1000).toFixed(1)}k`
  }
  if (result < 1000000000) {
    return `${(result / 1000000).toFixed(2)}m`
  }
  if (result < 1000000000000) {
    return `${(result / 1000000000).toFixed(3)}b`
  }
  if (result < 1000000000000000) {
    return `${(result / 1000000000000).toFixed(3)}t`
  }
  return result
}

export default {
  filters: {
    toLocaleString,
    toLocaleDateString,
    toDateDiffString,
    shortNumber
  }
}