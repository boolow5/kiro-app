import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import app from './modules/app'
import user from './modules/user'
import homes from './modules/homes'
import agent from './modules/agent'
import location from './modules/location'
import files from './modules/files'
import payment from './modules/payment'

const LocalStorage = (store) => {
  store.subscribe((mutation) => {
    window.localStorage.setItem('auth', JSON.stringify(user.state))
    if (mutation.type === 'CLEAR_ALL') {
      window.localStorage.removeItem('auth')
    }
  })
}

export default new Vuex.Store({
  modules: {
    app,
    user,
    homes,
    agent,
    location,
    files,
    payment
  },
  plugins: [LocalStorage]
})
